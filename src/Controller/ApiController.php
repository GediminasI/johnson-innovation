<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\MineLogs;
use App\Entity\Faq;
use App\Entity\User;
use App\Entity\Currency;

/**
* @Route("/api")
*/
class ApiController extends Controller {
    /**
     * @Route("/get-mining-logs", name="mining_logs")
     */
    public function index()
    {
        return $this->json([
            'success' => true,
            'message' => []
        ]);
    }

    /**
     * @Route("/dashboard", name="dashboard_api")
     */
    public function dashboardApi()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        if (!$user) {
            $user = $em->getRepository(User::class)->find(1);
        }

        $mineLogsRepository = $em->getRepository(MineLogs::class);

        $hashRate = $mineLogsRepository->findByLastTimeInterval($user->getId());
        $averageHashRate = $mineLogsRepository->findAverageHashRate($user->getId());

        $totalHashrate = 0;

        foreach ($hashRate as $hash) {
            $totalHashrate += $hash->getHashes();
        }

        $result['hashRate'] = $totalHashrate/5;
        $result['averageHashRate'] = $averageHashRate['average_hash'];

        $response = new JsonResponse($result, 200, [
            'Access-Control-Allow-Headers' => '*'
        ]);

        return $response;
    }

    /**
     * @Route("/currencies", name="currencies_show")
     */
    public function currenciesApi()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        if (!$user) {
            $user = $em->getRepository(User::class)->find(1);
        }

        $result = [
            'id' => $user->getId(),
            'currencies' => []
        ];

        $currencyRate = [
            'USD' => 1,
            'BTC' => 11166.33,
            'ETH' => 849.555020,
            'XMR' => 342.260995
        ];

        $change = [
            'USD' => 0,
            'BTC' => -2.29,
            'ETH' => -13.61,
            'XMR' => -0.98
        ];

        foreach ($user->getCurrencies() as $currency) {
            $result['currencies'][] = [
                'id' => $currency->getId(),
                'code' => $currency->getCurrency()->getCode(),
                'name' => $currency->getCurrency()->getName(),
                'icon' => $currency->getCurrency()->getIcon(),
                'amount' => $currency->getAmount(),
                'toUsd' => $currency->getAmount()*$currencyRate[$currency->getCurrency()->getCode()],
                'change' => $change[$currency->getCurrency()->getCode()],
                'rate' => $currencyRate[$currency->getCurrency()->getCode()],
                'hasAdded' => true
            ];
        }

        $response = new JsonResponse($result, 200, [
            'Access-Control-Allow-Headers' => '*'
        ]);

        return $response;
    }

    /**
     * @Route("/all-currencies", name="all_currencies")
     */
     public function allCurrencies()
     {
        $em = $this->getDoctrine()->getManager();

        $currencies = $em->getRepository(Currency::class)->findAll();

        $result = [];

        foreach ($currencies as $currency) {
            $result[] = [
                'id' => $currency->getId(),
                'code' => $currency->getCode(),
                'name' => $currency->getName(),
                'icon' => $currency->getIcon(),
                'feeSize' => $currency->getFeeSize()
            ];
        }

        $response = new JsonResponse($result, 200, [
            'Access-Control-Allow-Headers' => '*'
        ]);

        return $response;
     }

    /**
     * @Route("/faq", name="faq")
     */
    public function faqApi()
    {
        $em = $this->getDoctrine()->getManager();

        $faqRepository = $em->getRepository(Faq::class);

        $faq = $faqRepository->findAll();

        $result = ['faq' => []];

        foreach ($faq as $item) {
            $result['faq'][] = [
                'id' => $item->getId(),
                'question' => $item->getQuestion(),
                'answer' => $item->getAnswer()
            ];
        }

        $response = new JsonResponse($result, 200, [
            'Access-Control-Allow-Headers' => '*'
        ]);

        return $response;
    }

}
