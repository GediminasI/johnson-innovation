<?php

namespace App\Repository;

use App\Entity\Transfers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Transfers|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transfers|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transfers[]    findAll()
 * @method Transfers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransfersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Transfers::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('t')
            ->where('t.something = :value')->setParameter('value', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
