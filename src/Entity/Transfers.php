<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TransfersRepository")
 */
class Transfers
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    private $userFrom;

    private $userTo;

    private $currencyFrom;

    private $currencyTo;

    private $amount;

    private $createdAt;

    private $updatedAt;
    // add your own fields
}
