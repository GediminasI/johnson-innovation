<?php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserCurrencies", mappedBy="user")
     */
    private $currencies;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MineLogs", mappedBy="user")
     */
    private $mineLogs;

    public function __construct()
    {
        parent::__construct();
        $this->currencies = new ArrayCollection();
    }

    public function getCurrencies()
    {
        return $this->currencies;
    }

    public function setCurrencies($currencies)
    {
        $this->currencies = $currencies;

        return $this;
    }

    public function getMineLogs()
    {
        return $this->mineLogs;
    }

    public function setMineLogs($mineLogs)
    {
        $this->mineLogs = $mineLogs;

        return $this->mineLogs;
    }
}
