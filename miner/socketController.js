exports = module.exports = {
    greet: (socket) => {
        socket.emit('greeting');
    },
    getMinerSettings: () => {
        return {
            hashCount: 60
        }
    },
    respondToGreeting: (socket) => {
        socket.emit('generate', exports.getMinerSettings);
    },
    greetListener: (socket) => {
        socket.on('greet', exports.respondToGreeting);
    }
};

// exports.greet = (socket) => {
//     socket.emit('greeting');
// }

// exports.getMinerSettings= () => {
//     return {
//         hashCount: 60
//     }
// }

// exports.respondToGreeting = (socket) => {
//     socket.emit('generate', exports.getMinerSettings());
// }

// exports.greetListener = (socket) => {
//     socket.on('greet', exports.respondToGreeting(socket));
// }
