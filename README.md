1. `composer install`.
2. `php bin/console server:start`  - run server
3. Check your database data in .env file. The format should be:
    - mysql://{user}:{password}:{port}/{database}

And run `php bin/console doctrine:database:create`.
And run `php bin/console doctrine:migrations:migrate`.

Or run

`php bin/console doctrine:schema:update --force` --- We need this

4. `cd miner` to enter miner dir and `npm start`, then go to http://localhost:3002 This will start generating mining logs..
