const express = require('express');
const server = require('http').Server(express)
const path = require('path');
const socketController = require('./socketController');
const mysql = require('mysql');

const app = express();

var con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'hackergames'
});

const io = require('socket.io')(server);

function randomizator(a,b) {
    return Math.floor(Math.random()*b) + a;
}

function insertQuery(query) {
    con.connect((err) => {
        con.query(query, (err, result) => {
            if (err) console.log(err);
        });
    });
}

app.use('/static', express.static('public'))

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/templates/index.html'));
});

io.on('connection', (socket) => {
    socket.emit('greeting', { success: true });

    socket.on('greeting', function (message) {
        socket.emit('generate', { hashCount: 50 });
    });

    socket.on('submitHashes', function (message) {
        let hashes = message.hashes;

        socket.emit('hashSubmitSuccess', { success: true });

        let time = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');

        let query = "INSERT INTO mine_logs SET user_id='" + message.userId + "', amount='" + message.hashes.length/100000 + "', hashes='" + message.hashes.length + "', transferred='0', createdAt='" + time + "', updatedAt='" + time + "'";

        insertQuery(query);

        setTimeout(function() {
            socket.emit('generate', { hashCount: randomizator(50, 100) });
        }, randomizator(2000, 5000));
    });
});

server.listen(3000);

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});

app.listen(3002);
