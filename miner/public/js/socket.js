var socket = io('|server_url|');
var userId = |user_id|;

function generateHash(something) {
    return forge.md.sha256.create().update(something).digest().toHex();
}

function getUserData() {
    return {
        cpu: 1.2
    };
}

function mineHashes(settings) {
    var data = {
        userId: userId,
        hashes: []
    };

    for (var i = 0; i < settings.hashCount; i++) {
        data.hashes.push(generateHash(i));
    }

    socket.emit('submitHashes', data);
}

socket.on('greeting', function (message) {
    socket.emit('greeting', getUserData());
});

socket.on('generate', function (message) {
    mineHashes(message);
});

socket.on('hashSubmitSuccess', function (message) {});
