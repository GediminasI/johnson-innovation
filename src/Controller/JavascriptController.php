<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use MatthiasMullie\Minify;
use Symfony\Component\Finder\Finder;
use App\Entity\User;

class JavascriptController extends Controller {
    /**
     * @Route("/scripts/{uid}/miner.{extension}", name="miner_script")
     */
    public function index($uid, $extension) {
        $userRepository = $this->getDoctrine()->getRepository(User::class);

        if (!$userRepository->find($uid) || $extension != 'min.js') {
            return new Response(
                '',
                Response::HTTP_NOT_FOUND,
                array('content-type' => 'text/html')
            );
        }

        $minifier = new Minify\JS();

        $finder = new Finder();
        $finder->files()->in(__DIR__.'/../../miner/public/modules/forge/dist/')->name('forge.min.js');
        $finder->files()->in(__DIR__.'/../../miner/public/js/')->name('socket.js');
        $finder->files()->in(__DIR__.'/../../miner/public/modules/socket.io-client/dist/')->name('socket.io.slim.js');

        foreach ($finder as $file) {
            if ($file->getFileName() == 'socket.js') {
                $source = file_get_contents($file->getRealPath());

                $source = str_replace('|server_url|', 'http://'.$this->container->getParameter('host'), $source);
                $source = str_replace('|user_id|', $uid, $source);

                $minifier->add($source);

                continue;
            }

            $minifier->add($file->getRealPath());
        }

        return new Response(
            $minifier->minify(),
            Response::HTTP_OK,
            ['content-type' => 'text/javascript']
        );
    }
}
