<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Minelogs
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="App\Repository\MineLogsRepository")
 */
class MineLogs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="mineLogs")
     */
    private $user;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;


    /**
     * @var integer
     *
     * @ORM\Column(name="hashes", type="integer")
     */
    private $hashes;

    /**
     * @var bool
     *
     * @ORM\Column(name="transferred", type="boolean")
     */
    private $transferred;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    public function __toString()
    {
        return 'Mine Logs';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return MineLogs
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set transferred
     *
     * @param boolean $transferred
     *
     * @return MineLogs
     */
    public function setTransferred($transferred)
    {
        $this->transferred = $transferred;

        return $this;
    }

    /**
     * Get transferred
     *
     * @return boolean
     */
    public function getTransferred()
    {
        return $this->transferred;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return MineLogs
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return MineLogs
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return MineLogs
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return MineLogs
     */
    public function setHashes($hashes)
    {
        $this->hashes = $hashes;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getHashes()
    {
        return $this->hashes;
    }

}
