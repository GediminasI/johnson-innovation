<?php

namespace App\Repository;

use App\Entity\MineLogs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MineLogs|null find($id, $lockMode = null, $lockVersion = null)
 * @method MineLogs|null findOneBy(array $criteria, array $orderBy = null)
 * @method MineLogs[]    findAll()
 * @method MineLogs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MineLogsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MineLogs::class);
    }

    public function findByLastTimeInterval($userId)
    {
        return $this->createQueryBuilder('m')
            ->where('m.user = :userId')
            ->andWhere('m.createdAt > :last')
            ->setParameter('userId', $userId)
            ->setParameter('last', new \DateTime('-5 second'), \Doctrine\DBAL\Types\Type::DATETIME)
            ->getQuery()
            ->getResult();
    }

    public function findAverageHashRate($userId)
    {
        return $this->createQueryBuilder('m')
            ->select('avg(m.hashes) AS average_hash')
            ->where('m.user = :userId')
            ->andWhere('m.createdAt > :last')
            ->setParameter('userId', $userId)
            ->setParameter('last', new \DateTime('-1 hour'), \Doctrine\DBAL\Types\Type::DATETIME)
            ->getQuery()
            ->getOneOrNullResult();
    }
    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('m')
            ->where('m.something = :value')->setParameter('value', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
