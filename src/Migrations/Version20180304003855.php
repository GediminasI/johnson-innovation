<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180304003855 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE mine_logs (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, amount DOUBLE PRECISION NOT NULL, transferred TINYINT(1) NOT NULL, createdAt DATE NOT NULL, updatedAt DATE NOT NULL, INDEX IDX_6CB921C6A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mine_logs ADD CONSTRAINT FK_6CB921C6A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE user_currencies DROP FOREIGN KEY FK_90A3807F38248176');
        $this->addSql('DROP INDEX UNIQ_90A3807F38248176 ON user_currencies');
        $this->addSql('ALTER TABLE user_currencies ADD crreated_at VARCHAR(255) NOT NULL, DROP currency_id');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE mine_logs');
        $this->addSql('ALTER TABLE user_currencies ADD currency_id INT DEFAULT NULL, DROP crreated_at');
        $this->addSql('ALTER TABLE user_currencies ADD CONSTRAINT FK_90A3807F38248176 FOREIGN KEY (currency_id) REFERENCES currency (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_90A3807F38248176 ON user_currencies (currency_id)');
    }
}
